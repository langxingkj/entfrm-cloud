package com.entfrm.core.auth.config;

import com.entfrm.core.auth.properties.SecurityProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * @author entfrm
 * @date 2019/10/7
 */
@EnableConfigurationProperties(SecurityProperties.class)
public class SecurityPropertiesConfig {
}
