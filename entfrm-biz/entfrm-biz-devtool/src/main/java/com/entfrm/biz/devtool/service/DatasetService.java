package com.entfrm.biz.devtool.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entfrm.biz.devtool.entity.Dataset;

public interface DatasetService extends IService<Dataset> {

}
