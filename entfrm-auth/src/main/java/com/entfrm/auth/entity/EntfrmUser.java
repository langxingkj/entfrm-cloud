package com.entfrm.auth.entity;

import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.social.security.SocialUserDetails;

import java.util.Collection;

/**
 * @author entfrm
 * @date 2020/3/10
 *
 * 扩展用户信息
 */
public class EntfrmUser extends User implements SocialUserDetails {
	/**
	 * 用户ID
	 */
	@Getter
	private Integer id;
	/**
	 * 部门ID
	 */
	@Getter
	private Integer deptId;

	/**
	 * 部门ID
	 */
	@Getter
	private String openId;

	public EntfrmUser(Integer id, Integer deptId, String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
		this.id = id;
		this.deptId = deptId;
	}

	@Override
	public String getUserId() {
		return this.openId;
	}
}
